# Generating Permutations and Combinations

Given (2, 7, 3, 6, 5, 4, 1), to find the next permutation:

1. From right, move cursor A until the item is less than the one on its right.
2. From right, move cursor B until the item is larger than A's.
3. Swap A and B.
4. Sort numbers after A or B in increasing order.

---

Binomial Theorem:

$$
\begin{aligned}
(a+b)^n
&= \sum_{k=0}^n \left(\begin{matrix}n \\ k\end{matrix}\right) a^{n-k} b^k \\ 
&= \sum_{k=0}^n \left(\begin{matrix}n \\ k\end{matrix}\right) a^k b^{n-k}\\
\end{aligned}
$$

---

Pigeonhole principle on the least $k$ number of elements to guarantee picking $b$:

$$
\begin{aligned}
n = \left\lceil k(b-1) + 1 \right\rceil
\end{aligned}
$$

---

Write $n$-subsets:

(total in set)! / ((pick length)! (total - pick)!)

$$
\begin{aligned}
\text{set} &= \{0, 1, 2, 3, 4\}, \text{ pick 4} \\
C(5, 4) &= \frac{5!}{4!1!}
\end{aligned}
$$