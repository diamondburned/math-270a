- Adjacent to: has edge to.
- Endpoint are 2 ends of an edge.
- Edge $\{e, c\}$ is incident to vertices $c$ and $e$.
- Neighboring vertices: v. that are connected using edges.
- Regular: all nodes have same degree.
- Degrees: edges coming out from a vertex.
- Sum of degrees = 2 * edges

---

- Cycles are also circuits.
- Trails are also paths.

---

- $K_n$: Complete: edge for all vertices (sometimes called n-clique)
- $C_n$: Cycle: connects in circle to each other
- $Q_n$: n-dimensional hypercube: connect if 1 bit difference
- $K_{n, m}$: 2 sets of vertices len $n$ and $m$ all connected to each other

---

Adjacency list:

- $a \rightarrow b, c$ (${a, b}, {a, c}$)
- $b \rightarrow a, c, e$ (${b, a}, {b, c}, {b, e}$)
- ...

Matrix:

|       | 1 | 2 | 3 | 4 | 5 |
| ----- | - | - | - | - | - |
| **1** | 0 | 1 | 1 | 0 | 0 |
| **2** | 1 | 0 | 1 | 0 | 1 |
| **3** | 1 | 1 | 0 | 1 | 0 |
| **4** | 0 | 0 | 1 | 0 | 1 |
| **5** | 0 | 1 | 0 | 1 | 0 |

Note that the lines connect diagonally, e.g. $5$ works in both
row or column.

---

- Graph isomorphism: basically if mapped correctly and the edges are the same.

<!-- ![](2021-10-11-22-02-29.png) -->
![](2021-10-11-22-02-49.png)

---

- A singel vertex walk has a length 0. $\langle A \rangle$.
- A connected component has all connected vertices. One that doesn't is not maximal.
- A connected vertex has a path across it.
- A graph is $k$-vertex-connected ($\kappa(G)$) if it:
    - contains at least $k+1$ vertices, and
    - remains connected after $k-1$ vertices are removed.
    - In other words, $k$ vertices have to be removed to disconnect the graph.
- A graph is $k$-edge-connected ($\lambda(G)$) if it:
    - remains connected after $k-1$ edges are removed.
  -  In other words, $k$ edges have to be removed to disconnect the graph.

Let min(degrees) be $\delta(G)$. Then,

$$
\begin{aligned}
\kappa(G) &\leq \delta(G) \\
\lambda(G) &\leq \delta(G)
\end{aligned}
$$

---

- Euler curcuit: walking through every vertex and edge (occur once).
  - If an undirected graph has n euler circuit, then it's connected and every vertex
  has an even degree.

Procedure for finding Euler circuit:

```
create circuit C being any circuit of graph G

while G is not circuit:
        create graph H of unused edges from graph G, no lone vertices
        pick any vertex R from both graph H and circuit C
        create circuit CH in graph H from vertex R
        C = combine circuit CH and circuit C

define combine(circuit C, circuit CH):
        walk C until a vertex R is in both C and CH
        walk from that vertex R in CH until end
        continue C from vertex R
```

- Euler trail: open trail that includes every edge (no repeats)
  - If an undirected graph is connected and has exactly 2 vetices w/ odd degree, then
  it has an Euler trial.
  - Trail begins and ends with the vertices of odd degrees.

---

- Hamiltonian cycle includes every vertex, only 1 repeat for 2 ends.
  - Turn into Hamiltonian path by deleting the last vertex.
- Hamiltonian path includes every vertex, no repeats.


- Graphs w/ a 1-degree vertex has no Hamiltonian **cycle**.
- $K_n$ graphs have a Hamiltonian **cycle** if $n \geq 3$.