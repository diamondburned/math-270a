|  Operation             |  Notation  |  Description                     |
|------------------------|------------|----------------------------------|
|  Intersection          |  A ∩ B     |  $\{ x : x ∈ A \text{ and } x ∈ B \}\\$         |
|  Union                 |  A ∪ B     |  $\{ x : x ∈ A \text{ or } x ∈ B \text{ or  both} \}\\$  |
|  Difference            |  A - B     |  $\{ x : x ∈ A \text{ and } x ∉ B \}\\$         |
|  Symmetric difference  |  A ⊕ B     |  $\{ x : x ∈ A - B \text{ or } x ∈ B - A \}\\$  |
|  Complement            |  A         |  $\{ x : x ∉ A \}\\$                   |

---

Set Identity
---

$$
\begin{aligned}
x \in A \cap B &\leftrightarrow (x \in A) \land (x \in B) \\
x \in A \cup B &\leftrightarrow (x \in A) \lor (x \in B) \\
x \in \overline A &\leftrightarrow \lnot (x \in A)
\end{aligned}
$$

![](2021-09-18-14-06-03.png)

---

Domain is the inputs. Range is the outputs. Target is all the possible outputs.

Function $f: A \rightarrow B$ is:
- One-to-one/injective if $f$ map to unique $B$ for every $A$.
- Onto/surjective if for all $B$, there's always an $A$ that maps to it.

Both is bijection.

Function is inversible iff it's bijective.

---

$$
\begin{aligned}
|X \times Y| &= |X| \cdot |Y| \\
|X \times Y \times Z| &= |X| \cdot |Y| \cdot |Z|
\end{aligned}
$$