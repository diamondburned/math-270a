For $r$-permutations with $n$ elements:

$$
\begin{aligned}
P(n, r) = \frac{n!}{(n-r)!}
\end{aligned}
$$

Number of $n$-subsets from total $t$:

$$
\begin{aligned}
\frac{P(t, n)}{n!}
\end{aligned}
$$

Number of $r$-subsets from $S$:

$$
\begin{aligned}
r_\text{subsets} = \frac{r_\text{permutations from S}}{r!} = \frac{P(n, r)}{r!}
\end{aligned}
$$

$n$ choose $r$:

$$
\begin{aligned}
\left(\begin{matrix}n \\ r\end{matrix}\right) =
\left(\begin{matrix}n \\ n - r\end{matrix}\right) =
 \frac{n!}{r!(n-r)!} \\
\end{aligned}
$$

---

Assume all variables are non-negative. The number of solutions
can be calculated with:

$$
\begin{aligned}
x_1 + x_2 + x_3 + x_4 &= 12 \\
\left(\begin{matrix}12+4-1 \\ 4-1\end{matrix}\right) &= 
\left(\begin{matrix}15 \\ 3\end{matrix}\right)
\end{aligned}
$$

When orders matter, we use permutations. Otherwise, we use subsets ($C$).

See 10.10.

|                         | No restrictions (any positive m and n)                  | At most one ball per bin (m must be at least n) | Same number of balls in each bin(m must evenly divide n) |
| ----------------------- | ------------------------------------------------------- | ----------------------------------------------- | -------------------------------------------------------- |
| Indistinguishable balls | $\left(\begin{matrix}n+m-1 \\ m - 1\end{matrix}\right)$ | $\left(\begin{matrix}m \\ n\end{matrix}\right)$ | 1                                                        |
| Distinguishable balls   | $m^n$                                                        |  $P(m, n)$                                               |                                             $\frac{n!}{\left(\frac{n}m!\right)^m}$             |

---

Cardinality of set union:

$$
\begin{aligned}
|A \cup B|&= |A| + |B| - |A  \cap B| \\
|A \cup B \cup C| &= |A| + |B| + |C| - |A  \cap B| - |B \cap C| - |A \cap C| + |A \cap B \cap C|| \\
\end{aligned}
$$

Can also use complement:

Let $U$ be the universal set.

$$
\begin{aligned}
|U| - |\overline{A \cup B}| = |A \cup B|
\end{aligned}
$$

Example: Let

- $U$ be all 4 digit PINs
- $P_n$ for the $n$-th pin digit with an 8

$$
\begin{aligned}
|P_1 \cup P_2 \cup P_3 \cup P_4| = |U| - |\overline{P_1 \cup P_2 \cup P_3 \cup P_4}|
\end{aligned}
$$

Magnitude of $U$ is $10^4$: any 10 digits repeated 4 times.

Magnitude of the negation: all possible digits except 8, therefore 9 possible combinations,
giving us $9^4$.

Union magnitude is $10^4 - 9^4 = 3439$

---

**Mutually disjoint**: if the intersections are empty, e.g.

- A = `100**`
- B = `*100*`
- C = `**100`
  
There's no $A \cap B$, because the 2nd bit is different; same goes for B and C.
Therefore, the total combination sum doesn't have subtractions (4 different values
(cardinality) per string, $4 \times 3 = 12$).

---

Permuting letters in words:

Given "SUBSETS", the permutation is $\frac{7!}{3!}\\$
Since we have 7 total letters with 3 repeating letters (S). The repetitions are multiplied.