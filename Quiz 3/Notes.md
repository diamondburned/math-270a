Pattern matching:

![](recurrence-eqns.png)

ch 14, ch 8.1-8.5

- **Geometric Sequence**: term = previous term * ratio
- **Arithmetic Sequence**: term = previous term + difference