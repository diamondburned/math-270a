- Reflexive: if ALL nodes reference itself $\forall x \in A, (x, x) \in R$.
- Anti-reflexive: if NO nodes reference itself $\forall x \in A, (x, x) \not \in R$.
- Neither: mix of both.

---

- Symmetric: if all nodes reference back on the other node OR has no reference.
    - $\forall x, y \in A$, if $(x, y) \in R$ then $(y, x) \in R$
- Anti-symmetric: no nodes reference back.
    - $\forall x, y \in A$, if $(x, y) \in R$ and $(y, x) \in R$, then $x = y$, or
    - $\forall x, y \in A$, if $x \not= y$, then $(x, y) \not \in R$ or $(y, x) \not \in R$

---

- Transitive: for all nodes, A -> B, B -> C, then A -> C must be true.
    - $\forall x, y, z \in A$, if $(x, y) \in R$ and $(y, z) \in R$, then $(x, z) \in R$.
- Not transitive: if that's not true.

---

- In-degree: input edges
- Out-degree: output edges

---

- Open-walk: didn't walk back to the start
- Closed-walk: walk back to the start, `n[0] == n[-1]`
- Walk length: nodes walked - 1

---

- Trail: open walk, edges occur once
- Path: open walk, edges and vertices occur once
- Circuit: closed walk, edges occur once
- Cycle: closed walk, edges and vertices occur once EXCEPT first and last

---

- Given $S \circ R$ on set $A$, $(a, c) \in S \circ R$ iff $b \in A$ such that $(a, b) \in R$ and $(b, c) \in S$
    - In other words, $a \rightarrow c$ only when $a \rightarrow b$, $b \rightarrow c$
- Order matters: if $S \circ R$, then check $R$ first, then $S$.

---

- An equivalence relation is reflexive, symmetric, and transitive: $a \sim b$.
