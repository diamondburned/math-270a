Suppose you have a biased coin that lands on heads 60% of the time,
and on tails 40% of the time. If you flip the coin 3 times, what is the
probability that:

a) it lands on headsd exactly 2/3 tosses, and

b) it lands on heads exactly 2/3 tosses given that first toss is head?

Just write all combinations out...