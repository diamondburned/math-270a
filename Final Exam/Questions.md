1. Recurrence Relation
    - $b_n = 3b_{n-1} + 10b_{n-2},$
2. Pigeonhole Principle
    - Just draw every possible combination out.
3. Probability
|                         | no restrictions                  | at most one/bin | same number each bin |
| ----------------------- | ------------------------------------------------------- | ----------------------------------------------- | -------------------------------------------------------- |
| Indistinguishable | $\left(\begin{matrix}n+m-1 \\ m - 1\end{matrix}\right)$ | $\left(\begin{matrix}m \\ n\end{matrix}\right)$ | 1                                                        |
| Distinguishable   | $m^n$                                                        |  $P(m, n)$                                               |                                             $\frac{n!}{\left(\frac{n}m!\right)^m}$             |
4. Conditional Probability
    - Bayes' Theorem: $p(F|X) = \frac{p(X|F)\ p(F)}{p(X|F) \ p(F) + p(X|\overline F) \ p(\overline F)},$
    - Grow trees, must be balanced
    - $p(A \text{\&} B) =P(A|B) \cdot P(B) = P(B|A) \cdot P(A)$
5. Reflexive, symmetric, anti-symmetric, transitive, equivalence relation
    - Reflexive: all nodes reference itself
    - Symmetric: all nodes reference back or has no reference
    - Transitive: $A \rightarrow B \rightarrow C$ then $A \rightarrow C$
6. Euler circuit/trail, Hamiltonian cycle/path
    - Cycle/path: edges+vertices once; circuit/trail edges once
    - Euler circuit algorithm: walk, remove used edges, repeat, combine
    - Hamiltonian cycle/path: include every vertex, vertex and edge once
7. Even/odd integer proof
    - $2n+1$ odd, $2n$ even.
    - Let $n$ be a positive integer etc. etc.
8. Set identity proof (fuck me)
9. Properties of functions (one-to-one, onto)
    - One-to-one/injective if $f$ map to unique $B$ for every $A$.
    - Onto/surjective if for all $B$, there's always an $A$ that maps to it.
10. Mathematical induction
    1. Prove the base case, lowest number
    2. Prove that if $n$, then $n+1$ holds true
        - Start by splitting out the last term

Goal: >24%! - 3 questions.