Check proof by contrapositive, r is irrat, sqrt(r) is irrat.

Review matrix orientation, practice transitive and symmetric relationship,
practice one-to-one and onto.

---

- Prime: has only 2 factors: 1 and itself.
- Composite: more than 2 factors.
- Neither: 1 factor. (1)

---

Matrix is vertical-first.