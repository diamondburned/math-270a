# Rules of inference

**Modus ponens**: hypothesis implies conclusion + hypothesis, therefore conclusion.

$$
\begin{aligned}
&p \rightarrow q \\
&p \\
&\therefore q
\end{aligned}
$$

**Modus tollens**: Hypothesis implies conclusion, not hypothesis, therefore not conclusion

$$
\begin{aligned}
&p \rightarrow q \\
&\lnot p \\
&\therefore \lnot q
\end{aligned}
$$

**Disjunctive syllogism**: one or the other; if it's not one, then it's the other.

$$
\begin{aligned}
& p \lor q \\
& \lnot p \\
& \therefore q
\end{aligned}
$$

**Simplification**: if A and B are true, then either of them is true.

$$
\begin{aligned}
& p \land q \\
&\therefore p \\
\end{aligned}
$$

$$
\begin{aligned}
& p \land q \\
&\therefore q \\
\end{aligned}
$$