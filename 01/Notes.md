| Connective     | Symbol            |
|----------------|-------------------|
| AND            | $\land$           |
| OR             | $\lor$            |
| NOT            | $\lnot$           |
| if then        | $\to$             |
| if and only if | $\leftrightarrow$ |


| $p$ |$q$ | $p \to q$ |
| - | - | ------ |
| x | T | T      |
| T | F | F      |
| F | F | T      |


| $p$ | $q$ | $p \leftrightarrow q$ |
| - | - | ------ |
| T | T | T      |
| T | F | F      |
| F | T | F      |
| F | F | T      |

## De Morgan's law

$$
\begin{aligned}
\lnot (p \lor q) &\equiv (\lnot p \land \lnot q) \\[1em]
\lnot \forall x P(x) &\equiv \exists x \lnot P(x) \\
\lnot \exists x P(x)  &\equiv \forall x \lnot P(x) \\[1em]
\lnot \forall x \forall y P(x, y) &\equiv \exists x \exists y \lnot P(x, y) \\
\lnot \forall x \exists y P(x, y) &\equiv \exists x \forall y \lnot P(x, y) \\
\lnot \exists x \forall y P(x, y) &\equiv \forall x \exists y \lnot P(x, y) \\
\lnot \exists x \exists y P(x, y) &\equiv \forall x \forall y \lnot P(x, y) \\
\end{aligned}
$$



## Everything

| Law                    | OR                                      | AND                                     |
| ---------------------- | --------------------------------------- | --------------------------------------- |
| Idempotent             | $p ∨ p ≡ p$                             | $p ∧ p ≡ p$                             |
| Associative            | $( p ∨ q ) ∨ r ≡  p ∨ ( q ∨ r )$        | $( p ∧ q ) ∧ r ≡  p ∧ ( q ∧ r )$        |
| Commutative            | $p ∨ q ≡ q ∨ p$                         | $p ∧ q ≡ q ∧ p$                         |
| Distributive           | $p ∨ ( q ∧ r ) ≡ ( p ∨ q ) ∧ ( p ∨ r )$ | $p ∧ ( q ∨ r ) ≡ ( p ∧ q ) ∨ ( p ∧ r )$ |
| Identity               | $p ∨ F ≡ p$                             | $p ∧ T ≡ p$                             |
| Domination             | $p ∧ F ≡ F$                             | $p ∨ T ≡ T$                             |
| Double negation law:   | $¬¬p ≡ p$                               |                                       |
| Complement             | $p ∧ ¬p ≡ F  ¬T ≡ F$                    | $p ∨ ¬p ≡ T  ¬F ≡ T$                    |
| De Morgan's            | $¬( p ∨ q ) ≡ ¬p ∧ ¬q$                  | $¬( p ∧ q ) ≡ ¬p ∨ ¬q$                  |
| Absorption             | $p ∨ (p ∧ q) ≡ p$                       | $p ∧ (p ∨ q)  ≡ p$                      |
| Conditional identities | $p → q ≡ ¬p ∨ q$                       | $p ↔ q ≡ ( p → q ) ∧ ( q → p )$      |

$$
\begin{aligned}
\boxed{\lnot (p \to q) \equiv p \land \lnot q}
\end{aligned}
$$

![](2021-08-25-16-01-42.png)

![](2021-08-25-17-29-49.png)

## Predicates and Quantifiers

For all: $\forall$

Exists: $\exists$

---

Nested quantifiers: expr. with more than one quantifier

It is a proposition if all quantifiers are bound (i.e. have a condition on the lhs).

---

## Rules of inference with propositions

| Rule of inference                         | Name                   |
| ----------------------------------------- | ---------------------- |
| $$p \\ p → q \\ \therefore q$$           | Modus ponens           |
| $$¬q \\ p → q \\ \therefore ¬p$$         | Modus tollens          |
| $$p \\ \therefore p ∨ q$$                 | Addition               |
| $$p ∧ q \\ \therefore p$$                 | Simplification         |
| $$p \\ q  \\ \therefore p ∧ q$$           | Conjunction            |
| $$p → q \\ q → r \\ \therefore p → r$$ | Hypothetical syllogism |
| $$p ∨ q \\ ¬p \\ \therefore q$$           | Disjunctive syllogism  |
| $$p ∨ q  \\ ¬p ∨ r \\ \therefore q ∨ r$$  | Resolution             |


| Rule of Inference                                                           | Name                       |
| --------------------------------------------------------------------------- | -------------------------- |
| $$c \text{ is an element (arbitrary or particular)} \\  ∀x P(x) \\ ∴ P(c)$$ | Universal instantiation    |
| $$c \text{ is an arbitrary element} \\ P(c) \\ ∴ ∀x P(x)$$                  | Universal generalization   |
| $$∃x P(x) \\ ∴ (c \text{ is a particular element}) ∧ P(c)$$                 | Existential instantiation  |
| $$c \text{ is an element (arbitrary or particular)} \\ P(c) \\ ∴ ∃x P(x)$$  | Existential generalization |