- 13.3: isomorphism between two graphs
    - check vertex w/ the highest degree and match them up
- 13.6: euler circuits ~~and trails~~
    - every vertex has an even degree
    - every edge covered once
    - see the algorithm
        - pick any vertex and walk a circuit
        - pick out unwalked edges and walk that again
- 13.7: hamilton cycles
    - cycle: no repeating vertices
    - need odd vertex
- 14.4.2: tree traversal, pre-order, no "part B (post-order)"
- 14.6: minimum spanning trees
    - Prim's algorithm

![](sections.png)

See Pigeonhole principle applications/use cases, dealing cards.