# 2.1. Mathematical Definitions

- **Parity**: odd or even; same parity if both are odd or even, otherwise opposite.
- **Rational numbers**: $\exists x, y$ where $y \not= 0$ and $r = x/y$.
- **Divides**: given $x, y$, $x$ divisible to $y$ iff $x \not= 0$ and $y = kx$. Denotes as $x | y$
    - If $x|y$, then $y$ is a multiple of $x$, and $x$ is a factor/divisor of $y$.
    - $3|12$ works; $12|3$ does not; $x > y$ then $x \not| y
- **Prime** $n > 1$, for all $m$, ($m=1$) or ($m|n$ and $m = n$)
- **Composite** $n > 1$, exists $m$ that $1 < m < n$ and $m | n$
    - If $n > 1$, it is either prime or composite
- **Rational**: exists $\frac{n}d$ where $n$ and $d$ are ints with no common factors other than 1, and $d \not= 0$.
    - The specification is required to ensure that the fraction is simplified.

Given $p \to q$, e.g. for all integers $n$, $n^2$ is odd, so $n$ is also odd.

- Direct: $p \to q$
- Contrapositive: $\lnot q \to \lnot p$ ($n$ is even, so $n^2$ is even)
- Contradiction: $p \land \lnot q$ ($n^2$ is odd and $n$ is even, therefore contradictory)