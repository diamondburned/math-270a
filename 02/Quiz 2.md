[1.12], [2.4, 2.5], [2.6, 2.7]

## 1.12

| Rule of inference                         | Name                   |
| ----------------------------------------- | ---------------------- |
| $$p \\ p → q \\ \therefore q$$           | Modus ponens           |
| $$¬q \\ p → q \\ \therefore ¬p$$         | Modus tollens          |
| $$p \\ \therefore p ∨ q$$                 | Addition               |
| $$p ∧ q \\ \therefore p$$                 | Simplification         |
| $$p \\ q  \\ \therefore p ∧ q$$           | Conjunction            |
| $$p → q \\ q → r \\ \therefore p → r$$ | Hypothetical syllogism |
| $$p ∨ q \\ ¬p \\ \therefore q$$           | Disjunctive syllogism  |
| $$p ∨ q  \\ ¬p ∨ r \\ \therefore q ∨ r$$  | Resolution             |

## 2.4

Direct proofs; see [Notes.md](Notes.md).

## 2.5

Contrapositive proofs; see [Notes.md](Notes.md).

## 2.6

Contradiction proofs; see [Notes.md](Notes.md).

## 2.7

Proof by cases, e.g. for every real number $x$, $x \geq 0$.

Prove these cases:

- $x < 0$: multiply both sides by $x$ gives $x^2 > 0$ (mult. by neg. switches direction), therefore $x^2 \geq 0$.
- $x = 0$: multiply both sides by $x$ gives $x^2 = 0$, therefore $x^2 \geq 0$.
- $x > 0$: multiply both sides by $x$ gives $x^2 > 0$, therefore $x^2 \geq 0$.

Form (for first case):

> Assume $x < 0$ and show $x^2 \geq 0$.
> 
> Multiplying both sides of $x < 0$ by $x$ gives $x^2 > 0$, since
> multiplying by a negative switches the direction of inequality.
> 
> Since $x^2 > 0$, $x^2 \geq 0$.