- **Experiment** produces possible **outcomes**
- All possible outcomes is called a **sample space**
- A subset of a sample space is an **event**

---

Cards:

- Ranks: 2, 3, 4, 5, 6, 7, 8, 9, 10, J (for jack), Q (for queen), K (for king), A (for ace).
- Suits: ♠ (spades), ♣ (clubs), ♥ (hearts), ♦ (diamonds).
- Total: 13 ranks, 4 suits, 52 cards

---

Mutually exclusive union events are defined as:

$$
\begin{aligned}
p(E_1 \cup E_2) = p(E_1) + p(E_2)
\end{aligned}
$$

If two events are not mutually exclusive, then they're defined as:

$$
\begin{aligned}
p(E_1 \cup E_2) = p(E_1) + p(E_2) - p(E_1 \cap E_2)
\end{aligned}
$$

(Sidenote: the subtraction is to remove items counted twice.)

---

Conditional probability:

$$
\begin{aligned}
p(E|F) = \frac{p(E \cap F)}{p(F)}
\end{aligned}
$$

($E$ conditioned on $F$).

![](conditional-probability.png)

See: $p(n|F)$ where $n$ is the overlap.

See: 12.3.2.

$$
\begin{aligned}
p(E|F) + p(\overline E | F) = 1
\end{aligned}
$$

---

Independent events: when conditioning onto $E$ the event $F$, the outcome doesn't change.

See definition 12.3.2:

$$
\begin{aligned}
p(E \cap F) = p(E) \cdot p(F)
\end{aligned}
$$

Mutually exclusive probability is apparently this?

$$
\begin{aligned}
p(E \cap F) = p(E) + p(F)
\end{aligned}
$$

---

Mutual independence: fancier word for mutual exclusivity:

$$
\begin{aligned}
P(A \cap B \cap ...) = P(A) \cdot P(B) \cdot ...
\end{aligned}
$$

---

Bayes' Theorem:

$$
\begin{aligned}
p(F|X) = \frac{p(X|F)\ p(F)}{p(X|F) \ p(F) + p(X|\overline F) \ p(\overline F)}
\end{aligned}
$$

---

Expected value of random variable function $X(n)$ and cases $C_n$:

$$
\begin{aligned}
E[X] &= X(C_1) \ p(C_1) + X(C_2) \ p(C_2) + ... \\
&= \sum_{s \in S} X(s) p(s)
\end{aligned}
$$