- Pre-order traversal: vertex processed before its descendants 
- Post-order traversal: vertex processed after its descendants

---

- Spanning tree: subgraph that contains all vertices and is a tree.

---

- Weighted graph: graph with real number to each edge.
- Minimum spanning tree: tree with the lowest weight